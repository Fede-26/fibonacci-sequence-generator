#!/usr/bin/env python3

#intro (ascii art)

print("""


  ______ _ _                                _ 
 |  ____(_| |                              (_)
 | |__   _| |__   ___  _ __   __ _  ___ ___ _ 
 |  __| | | '_ \ / _ \| '_ \ / _` |/ __/ __| |
 | |    | | |_) | (_) | | | | (_| | (_| (__| |
 |_|    |_|_.__/ \___/|_| |_|\__,_|\___\___|_|


  ___  ___  __ _ _   _  ___ _ __   ___ ___ 
 / __|/ _ \/ _` | | | |/ _ | '_ \ / __/ _ \\
 \__ |  __| (_| | |_| |  __| | | | (_|  __/
 |___/\___|\__, |\__,_|\___|_| |_|\___\___|
              | |                 _         
              |_|                | |            
   __ _  ___ _ __   ___ _ __ __ _| |_ ___  _ __ 
  / _` |/ _ | '_ \ / _ | '__/ _` | __/ _ \| '__|
 | (_| |  __| | | |  __| | | (_| | || (_) | |   
  \__, |\___|_| |_|\___|_|  \__,_|\__\___/|_|   
   __/ |                                        
  |___/                                         


""")

#choose whether the sequence must also have the number
elegance = input("should I add elegance to the sequence? \n (Y)es or (N)o").lower()

#choose the metod to use
metod = input("which metod should i use? \n (F)ast but short or (E)asy but long").lower()

if (metod == "e"):  #easy metod but very slow
    def F(n):
        if n == 0: return 0
        elif n == 1: return 1
        else: return F(n-1)+F(n-2)

elif (metod == "f"):    #faster metod for computer but cannot go further than 605
    from math import sqrt
    def F(n):
        return ((1+sqrt(5))**n-(1-sqrt(5))**n)/(2**n*sqrt(5))

howLong = int(input("how long do you want the sequence?"))
i = 1

try:
    while (howLong >= i):
        if elegance == "n": print(int(F(i)))                            #if elegance is inactive print only the fibonacci number
        elif elegance == "y": print(str(i)+" : "+ str(int(F(i))))       #if eleg. is active print also the cardinal number of the fibonacci number
        i = i + 1

except(OverflowError):  #in case of an overflow error because the fast metod
    print("OVERFLOW ERROR")
    print("it is not possible to calculate a sequence longer than " + str(i))
    print("\n")
